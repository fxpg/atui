import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { PresentationComponent } from './presentation/presentation.component';
import { ComponentsComponent } from './components/components.component';
import { SectionsComponent } from './sections/sections.component';
import { UsageComponent } from './alphatrader/usage/usage.component';
import { AddproductComponent } from './alphatrader/addproduct/addproduct.component';
import { BlogpostComponent } from './alphatrader/blogpost/blogpost.component';
import { BlogpostsComponent } from './alphatrader/blogposts/blogposts.component';
import { ContactusComponent } from './alphatrader/contactus/contactus.component';
import { DiscoverComponent } from './alphatrader/discover/discover.component';
import { EcommerceComponent } from './alphatrader/ecommerce/ecommerce.component';
import { LandingComponent } from './alphatrader/landing/landing.component';
import { LoginComponent } from './alphatrader/login/login.component';
import { ProductpageComponent } from './alphatrader/productpage/productpage.component';
import { ProfileComponent } from './alphatrader/profile/profile.component';
import { RegisterComponent } from './alphatrader/register/register.component';
import { SearchComponent } from './alphatrader/search/search.component';
import { SettingComponent } from './alphatrader/setting/setting.component';
import { TwitterComponent } from './alphatrader/twitter/twitter.component';
import { Page404Component } from './alphatrader/page404/page404.component';
import { Page422Component } from './alphatrader/page422/page422.component';
import { Page500Component } from './alphatrader/page500/page500.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { TraderComponent } from './alphatrader/trader/trader.component';

const routes: Routes =[
    { path: '',         component: PresentationComponent },
    { path: 'login',       component: LoginComponent },
    { path: 'trader',       component: TraderComponent },
    { path: 'register',    component: RegisterComponent },
    { path: 'setting',    component: SettingComponent },
    { path: 'usage',    component: UsageComponent },
    { path: '', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
