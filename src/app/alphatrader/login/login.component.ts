import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService,ToasterService } from '../../shared/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    inputUserId: string;
    inputPassword: string;

    displayError: boolean = false;
    errorMessage: string;

    data : Date = new Date();
    block: boolean = false;
    hide = true;

    constructor(
        private userService: UserService,
        private toasterService: ToasterService,
        private router: Router,
    ) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('full-screen');
        body.classList.add('login');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
        if (navbar.classList.contains('nav-up')) {
            navbar.classList.remove('nav-up');
        }
    }
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('full-screen');
        body.classList.remove('login');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    async login() {
        this.block = true;
        try {
            const ok = await this.userService.login(this.inputUserId, this.inputPassword);
            if(!ok) {
                this.toasterService.showToaster('ログインに失敗しました')
                return;
            }
            this.router.navigate(['/']);
            this.toasterService.showToaster('ログインが完了しました')
        } finally {
            this.block = false;
        }
    }
}
