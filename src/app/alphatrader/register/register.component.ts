import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { UserService,ToasterService } from '../../shared/services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    inputUserId: string;
    inputPassword: string;
    inputPassword2: string;

    // block
    block: boolean = false;
    hide = true;

    data : Date = new Date();

    constructor(
      public location: Location,
      private userService: UserService,
      private toasterService: ToasterService,
      private router: Router,
    ) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('full-screen');
        body.classList.add('register');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    }
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('full-screen');
        body.classList.remove('register');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    async register() {
        console.log(this.inputUserId);
        if(this.inputPassword != this.inputPassword2) {
          this.toasterService.showToaster('パスワードが一致しません')
          return
        }
        try {
            const ok = await this.userService.createUser(this.inputUserId, this.inputPassword);
            if(!ok) {
                this.toasterService.showToaster('登録に失敗しました')
                return;
            }
            this.toasterService.showToaster('登録が完了しました')
            this.router.navigate(['/']);
        } finally {
        }
    }
}
