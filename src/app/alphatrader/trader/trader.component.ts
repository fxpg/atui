import { Component, OnInit,OnDestroy ,ViewChild} from '@angular/core';
import { Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { ChartService, ExchangeService, TraderService, UserService, ToasterService } from '../../shared/services';
import { Chart, Exchange, CurrencyPair, Trader,TraderStatus,Order,UserResource,Log } from '../../shared/models';
import { TimeHelper } from '../../shared/helpers/time-helper';
import { Router } from '@angular/router';
declare let d3: any;

@Component({
  selector: 'run-trader-dialog',
  template: `
  <h5>自動取引ボットを作成します</h5>
  <div style="margin:0 auto; margin-top: 30px; text-align: center;">
  <button mat-raised-button (click)="dialogRef.close(false)">キャンセル</button>
  <button mat-raised-button  color="accent" (click)="dialogRef.close(true)">作成</button>
  </div>`
})
export class RunTraderDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<RunTraderDialogComponent>) { }
}


@Component({
  selector: 'edit-trader-dialog',
  template: `
  <h5>自動取引ボットを変更します</h5>
  <div style="margin:0 auto; margin-top: 30px; text-align: center;">
  <button mat-raised-button (click)="dialogRef.close(false)">キャンセル</button>
  <button mat-raised-button  color="accent" (click)="dialogRef.close(true)">変更</button>
  </div>`
})
export class EditTraderDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<EditTraderDialogComponent>) { }
}


type modeEnum = "create" | "edit" | "view";
namespace Enum {
  export const create:modeEnum = "create"
  export const edit:modeEnum = "edit"
  export const view:modeEnum = "view"
}

@Component({
    selector: 'app-trader',
    templateUrl: './trader.component.html',
    styleUrls: ['./trader.component.css']
})
export class TraderComponent implements OnInit,OnDestroy {
    @ViewChild('editor') editor;
    ace_options:any = {maxLines: 1000, printMargin: true};
    data: any;
    isLoadingChart: boolean = true;
    isLoadingTraderInfo: boolean = true;

    options: any;
    code: string;
    codeDefault: string;

    codeMirrorConfig = {
        lineNumbers: true,
        mode: 'text/x-yaml',
    }

    traderId: number;

    displayDialog = false;
    dialogMessage: string;
    exchanges: Exchange[] = [];
    currencyPairs: CurrencyPair[] = [];
    selectedExchange: Exchange;
    selectedCurrencyPair: CurrencyPair;
    traderStatuses?: { traderId: number; status: string; terminateReason: string; description: string }[];
    traderStatus?: TraderStatus;
    orders?: Order[];
    logs?: Log[];

    selectedTickSec = "60";
    description = "";
    tickSecs = [];
    numberOfTraderAvailable?: number;
    interval: any;


    mode: modeEnum;


    constructor(
        private userService: UserService,
        private chartService: ChartService,
        private exchangeService: ExchangeService,
        private traderService: TraderService,
        private toasterService: ToasterService,
        private router: Router,
        public dialog: MatDialog,
    ) {
      this.mode = Enum.create;

      this.codeDefault = `def tick():
      charts = data.charts('poloniex','ETC_USDT',60,100)

      upper, middle, lower  = talib.BBANDS(charts.close.values, matype=talib.MA_Type.T3)

      if charts.close.values[-1] < lower[-1]:
          trade.buy('poloniex','ETC_USDT',1)
      elif upper[-1] < charts.close.values[-1]:
          trade.sell('poloniex','ETC_USDT',1)`;
        this.code = this.codeDefault;
      this.tickSecs = [
        { value: '60', display: '1分' },
        { value: '300', display: '5分' },
        { value: '600', display: '10分' },
        { value: '1800', display: '30分' }
      ];
        this.options = {
            chart:{
                type: 'candlestickBarChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 100,
                    left: 60
                },
                showValues: true,
                x: function(d){ return d['date']; },
                y: function(d){ return d['close']; },
                duration: 100,
                xAxis: {
                    tickFormat: function(d) {
                        return d3.time.format('%Y/%m/%d %H:%M:00')(new Date(d));
                    },
                    showMaxMin: false,
                    axisLabelDistance: -50,
                    rotateLabels: -45
                },
                yAxis: {
                    tickFormat: function(d){
                        return d3.format(',.6f')(d);
                    },
                    showMaxMin: false,
                    axisLabelDistance: -10
                },
                callback: function () {
                  d3.selectAll('.nv-tick.positive .nv-bars').style('fill', '#e91e63');
                  d3.selectAll('.nv-tick.negative .nv-bars').style('fill', '#3f51b5');
                  d3.selectAll('line.nv-candlestick-lines').style('stroke', 'white');
                  d3.selectAll('.nvd3 g.tick text').style('fill', "white");

                },
            }
        };
    }
    async updateTraderInfoWithInput() {
        this.isLoadingTraderInfo = true;
        if(this.selectedExchange == null) {
            this.exchanges = await this.exchangeService.getExchanges();
            this.selectedExchange = this.exchanges[0];
        }
        this.currencyPairs = await this.exchangeService.getCurrencyPairs(this.selectedExchange.id);
        this.isLoadingTraderInfo = false;
    }

    async ngOnInit() {
        if(this.userService.userSub.getValue() == null) {
          this.toasterService.showToaster("ログインが必要です。");
          this.router.navigate(['/login']);
        }
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('settings');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
        navbar.classList.add('bg-danger');

        this.refreshData();

        this.selectedExchange = this.exchanges[0];
        this.selectedCurrencyPair = this.currencyPairs[0];

        this.interval = setInterval(() => {
          this.refreshData();
        }, 10000);
    }


    refreshData() {
        this.updateTraderInfoWithInput();
        this.getTraders();
        this.getNumberOfTraderAvailable();
    }

    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('settings');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
        navbar.classList.remove('bg-danger');
        clearInterval(this.interval);
    }
    dialogRunTrader() {
      let dialogRef = this.dialog.open(RunTraderDialogComponent,{
        autoFocus :false,
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.runTrader()
        }
      });
    }
    async runTrader() {
        try {
            const trader = new Trader({
                description: this.description,
                code: this.code,
                tickSec: parseInt(this.selectedTickSec),
            });
            const res = await this.traderService.runTrader(trader);
            this.traderId = res.traderId;

        } finally {
          this.refreshData();
        }
    }
    async queueStopTrader(traderId:number) {
        const ok = await this.traderService.queueStopTrader(traderId);
        if(!ok) {
          this.toasterService.showToaster('取引ボットの停止に失敗しました');
          return
        }
        this.toasterService.showToaster('停止中です');
        this.getTraders();
    }
    async deleteTrader(traderId:number) {
        const ok = await this.traderService.deleteTrader(traderId);
        if(!ok) {
          this.toasterService.showToaster('取引ボットの削除に失敗しました');
          return
        }
        this.toasterService.showToaster('削除しました');
        this.getTraders();
    }
    async getTraders() {
        this.isLoadingTraderInfo = true;
        const res = await this.traderService.getTraders();
        this.traderStatuses = res
    }
    async getTrader(traderId:number) {
        const res = await this.traderService.getTrader(traderId);
        if(res instanceof TraderStatus) {
          this.traderStatus = res;
          const ords = await this.traderService.getOrders(traderId);
          this.orders = ords;
          const logs = await this.traderService.getLogs(traderId);
          this.logs = logs;

          this.code = res.code;
          this.description = res.description;
          this.selectedTickSec = String(res.tickSec);
          this.mode = Enum.edit;
        }
    }
    modeCreate() {
      this.mode = Enum.create;
      this.code = this.codeDefault;
      this.description = "";
      this.orders = null;
      this.logs = null;
      this.traderStatus = null;
    }

    dialogEditTrader(traderId: number) {
      let dialogRef = this.dialog.open(EditTraderDialogComponent,{
        autoFocus :false,
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.editTrader(traderId)
        }
      });
    }
    async editTrader(traderId: number) {
        try {
            const trader = new Trader({
                description: this.description,
                code: this.code,
                tickSec: parseInt(this.selectedTickSec),
            });
            const res = await this.traderService.patchTrader(traderId,trader);
        } finally {
          this.refreshData();
        }
    }

    async getNumberOfTraderAvailable() {
        const res = await this.userService.getUserResource();
        if(res instanceof UserResource) {
          this.numberOfTraderAvailable = res.traderMax - res.traderSaved;
        }
    }

    convertExchangeIdToName(x: number): string {
      for (const ex of this.exchanges) {
        if (ex.id == x) {
          return ex.name;
        }
      }
      return "";
    }
}
