import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { NouisliderModule } from 'ng2-nouislider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { AgmCoreModule } from '@agm/core';
import { ImageUploadModule } from '../shared/image-upload/image-upload.module';
import { AceEditorModule } from 'ng2-ace-editor';
import { RouterModule } from '@angular/router';

import { ExamplesComponent } from './alphatrader.component';
import { UsageComponent } from './usage/usage.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { BlogpostComponent } from './blogpost/blogpost.component';
import { BlogpostsComponent } from './blogposts/blogposts.component';
import { ContactusComponent } from './contactus/contactus.component';
import { DiscoverComponent } from './discover/discover.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { ProductpageComponent } from './productpage/productpage.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { SettingComponent } from './setting/setting.component';
import { TwitterComponent } from './twitter/twitter.component';
import { Page404Component } from './page404/page404.component';
import { Page422Component } from './page422/page422.component';
import { Page500Component } from './page500/page500.component';
import { TraderComponent,RunTraderDialogComponent,EditTraderDialogComponent } from './trader/trader.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-md';

import * as services from '../shared/services';

// Angular Material
import { MatSelectModule,   MatTableModule, MatInputModule,  MatButtonModule,  MatListModule,MatSnackBarModule, MatDialogModule } from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        TagInputModule,
        NouisliderModule,
        JWBootstrapSwitchModule,
        AngularMultiSelectModule,
        AgmCoreModule.forRoot({
            apiKey: ''
        }),
        MarkdownModule.forRoot(),
        ImageUploadModule,
        AceEditorModule,
        FormsModule,
        ReactiveFormsModule,

        RouterModule,
        MatSelectModule,
        MatInputModule,
        MatButtonModule,
        MatListModule,
        MatSnackBarModule,
        MatDialogModule,
        MatTableModule,
    ],
    declarations: [
        ExamplesComponent,
        UsageComponent,
        AddproductComponent,
        BlogpostComponent,
        BlogpostsComponent,
        ContactusComponent,
        DiscoverComponent,
        EcommerceComponent,
        LandingComponent,
        LoginComponent,
        ProductpageComponent,
        ProfileComponent,
        RegisterComponent,
        SearchComponent,
        SettingComponent,
        TwitterComponent,
        Page404Component,
        Page422Component,
        Page500Component,

        TraderComponent,
        RunTraderDialogComponent,
        EditTraderDialogComponent,
    ],

    providers: [
        services.HttpService,
        services.UserService,
        services.ChartService,
        services.ExchangeService,
        services.BacktestService,
        services.TraderService,
        services.ToasterService,
        services.SettingService,
      ],
    entryComponents: [RunTraderDialogComponent,EditTraderDialogComponent]
})
export class ExamplesModule { }
