import { Component, OnInit, OnDestroy ,ViewChild} from '@angular/core';
import { ChartService, ExchangeService, TraderService, UserService, ToasterService, HttpService, SettingService } from '../../shared/services';
import { Chart, Exchange, CurrencyPair, Trader,TraderStatus,Order,Setting } from '../../shared/models';
import { TimeHelper } from '../../shared/helpers/time-helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit,OnDestroy {
    state_info = true;
    state_info1 = true;
    state_info2 = true;
    block: boolean = false;
    hide = true;
    apiKey: string;
    secretKey: string;
    settings: Setting[] = [];
    exchanges: Exchange[] = [];
    selectedExchange: Exchange;
    interval: any;

    data : Date = new Date();

    constructor(
        private userService: UserService,
        private httpService: HttpService,
        private exchangeService: ExchangeService,
        private settingService: SettingService,
        private toasterService: ToasterService,
        private router: Router,
    ) {
    }

    async ngOnInit() {
      if(this.userService.userSub.getValue() == null) {
        this.toasterService.showToaster("ログインが必要です。");
        this.router.navigate(['/login']);
      }
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('settings');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
        navbar.classList.add('bg-danger');
        this.getSettings();
        await this.getExchanges();
        this.interval = setInterval(() => {
          this.refreshData();
        }, 10000);
    }
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('settings');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
        navbar.classList.remove('bg-danger');
        clearInterval(this.interval);
    }

    refreshData() {
      this.getSettings();
    }
    async getExchanges() {
      this.exchanges = await this.exchangeService.getExchanges();
      this.selectedExchange = this.exchanges[0];
    }

    async deleteSetting(setting_id:number) {
        try {
            const ok = await this.settingService.deleteSetting(setting_id);
            if(!ok) {
                this.toasterService.showToaster('apikeyの削除に失敗しました');
                return;
            }
            this.toasterService.showToaster('apikeyを削除しました');
        } finally {
            this.block = false;
            this.getSettings();
        }
    }

    async addSetting() {
        const setting = new Setting({
          settingId: null,
          exchangeId: this.selectedExchange.id,
          apiKey: this.apiKey,
          secretKey: this.secretKey,
        });

        try {
            const ok = await this.settingService.setSetting(setting);
            if(!ok) {
                this.toasterService.showToaster('追加に失敗しました');
            }else {
              this.toasterService.showToaster('設定を追加しました');
            }
        } finally {
            this.block = false;
            this.getSettings();
        }
        return;
    }

    async getSettings() {
        try {
            const settings = await this.settingService.getSettings();
            if (settings == null) {
              this.settings = []
            }
            this.settings = settings;
        } finally {
        }
    }
    convertExchangeIdToName(x: number): string {
      for (const ex of this.exchanges) {
        if (ex.id == x) {
          return ex.name;
        }
      }
      return "";
    }
}
