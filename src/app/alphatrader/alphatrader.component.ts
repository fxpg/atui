import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService, HttpService,ToasterService} from '../shared/services';
import { User } from '../shared/models';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-alphatrader',
  templateUrl: './alphatrader.component.html',
  styleUrls: ['./alphatrader.component.scss']
})
export class ExamplesComponent implements OnInit {

  constructor(
        private httpService: HttpService,
        private userService: UserService,
        private toasterService: ToasterService,
        private router: Router,
        private snackBar: MatSnackBar,
    ) {
    }

  ngOnInit() {
  }

    logout() {
        this.userService.logout();
        this.router.navigate(['/']);
        this.toasterService.showToaster('ログアウトが完了しました')
    }
}
