import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Exchange, CurrencyPair } from '../models';

@Injectable()
export class ExchangeService {

    constructor(private httpService : HttpService) { }

    public async getExchanges(): Promise<Exchange[]> {
        return await this.httpService.getExchanges();
    }

    public async getCurrencyPairs(exchangeId: number): Promise<CurrencyPair[]> {
        return await this.httpService.getCurrencyPairs(exchangeId);
    }
}
