import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Chart } from '../models';

@Injectable()
export class ChartService {

    constructor(private http: HttpService) { }


    public async getCharts(exchangeId: number, duration: number,
                           trading: string, settlement: string,
                           start: Date, end: Date): Promise<Chart[]> {
        return await this.http.getCharts(exchangeId, duration,
                                         trading, settlement, start, end);
    }
}
