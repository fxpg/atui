import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Error, User } from '../models';
import { Subject, BehaviorSubject } from 'rxjs';
import { UserResource } from '../models';

@Injectable()
export class UserService {
    private storage: Storage;
    private STORAGE_TOKEN_KEY = "token";
    private STORAGE_USER_ID = "user_id";

    public userSub: BehaviorSubject<User> = new BehaviorSubject<User>(null);

    constructor(private http: HttpService) {
        this.storage = localStorage;
        this.http.errorSubject.subscribe((err: Error) => {
            if(err.errorCode == "E00001") {
                this.clearUserInfo();
            }
        });
    }

    public async createUser(userId: string, password: string): Promise<boolean> {
        await this.http.createUser(userId, password);
        await this.login(userId, password);
        return true;
    }

    public async resumeSession() {
        const userId = this.storage.getItem(this.STORAGE_USER_ID);
        const token = this.storage.getItem(this.STORAGE_TOKEN_KEY);
        if(userId !== null && token !== null) {
            const user = new User();
            user.userId = userId;
            this.userSub.next(user);
        }
    }

    public async login(userId: string, password: string): Promise<boolean> {
        const token = await this.http.login(userId, password);
        this.storage.setItem(this.STORAGE_TOKEN_KEY, token);
        this.storage.setItem(this.STORAGE_USER_ID, userId);

        const user = new User();
        user.userId = userId;
        this.userSub.next(user);

        return true;
    }

    public async getUserResource(): Promise<UserResource> {
        const userId = this.storage.getItem(this.STORAGE_USER_ID);
        const token = this.storage.getItem(this.STORAGE_TOKEN_KEY);
        return await this.http.getUserResource(userId, token);
  }


    public logout() {
        this.clearUserInfo();
    }

    public clearUserInfo() {
        this.storage.removeItem(this.STORAGE_TOKEN_KEY);
        this.storage.removeItem(this.STORAGE_USER_ID);
        this.userSub.next(null);
    }

    public getUserId(): string {
        return this.storage.getItem(this.STORAGE_USER_ID) || "none";
    }

    public getToken(): string {
        return this.storage.getItem(this.STORAGE_TOKEN_KEY) || "none";
    }
}
