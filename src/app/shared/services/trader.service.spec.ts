import { TestBed, inject } from '@angular/core/testing';

import { traderService } from './trader.service';

describe('traderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [traderService]
    });
  });

  it('should be created', inject([traderService], (service: traderService) => {
    expect(service).toBeTruthy();
  }));
});
