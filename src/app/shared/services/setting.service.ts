import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { UserService } from './user.service';
import { Setting } from '../models';

@Injectable()
export class SettingService {

      constructor(private httpService: HttpService,
                  private userService: UserService) { }

    public async setSetting(setting: Setting): Promise<boolean> {
        return await this.httpService.setSetting(setting,
          this.userService.getUserId(),
          this.userService.getToken()
        );
    }
    public async getSettings(): Promise<Setting[]> {
        return await this.httpService.getSettings(
          this.userService.getUserId(),
          this.userService.getToken()
        );
    }
    public async deleteSetting(setting_id: number): Promise<boolean> {
        return await this.httpService.deleteSetting(setting_id,
          this.userService.getUserId(),
          this.userService.getToken()
        );
    }
}
