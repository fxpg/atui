import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Error, User, Chart, Exchange, CurrencyPair, Backtest, BacktestResult, BacktestStatus,Setting, Trader,TraderStatus,Order,UserResource, Log } from '../models';
import { TimeHelper } from '../helpers/time-helper';
import { environment } from '../../../environments/environment';

@Injectable()
export class HttpService {
    private baseUrl = environment.apiserver;
    public errorSubject = new Subject<Error>();

    constructor(private http : HttpClient) {
    }

    private doGet(path: string, options?: {
        headers?: HttpHeaders,
        params?: HttpParams
    }): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.get(this.baseUrl + path, options)
                .subscribe(data => {
                    resolve(data);
                }, (err: HttpErrorResponse) => {
                    const error = new Error(err.error.error_code, err.error.error_message);
                    this.errorSubject.next(error);
                });
        });
    }

    private doPost(path: string, body?: any, options?: {
        headers?: HttpHeaders,
        params?: HttpParams
    }): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post(this.baseUrl + path, body, options)
                .subscribe(data => {
                    resolve(data);
                }, (err: HttpErrorResponse) => {
                    const error = new Error(err.error.error_code, err.error.error_message);
                    this.errorSubject.next(error);
                });
        });
    }
    private doDelete(path: string, body?: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete(this.baseUrl + path, body)
                .subscribe(data => {
                    resolve(data);
                }, (err: HttpErrorResponse) => {
                    const error = new Error(err.error.error_code, err.error.error_message);
                    this.errorSubject.next(error);
                });
        });
    }

    private doPatch(path: string, body?: any, options?: {
        headers?: HttpHeaders,
        params?: HttpParams
    }): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.patch(this.baseUrl + path, body, options)
                .subscribe(data => {
                    resolve(data);
                }, (err: HttpErrorResponse) => {
                    const error = new Error(err.error.error_code, err.error.error_message);
                    this.errorSubject.next(error);
                });
        });
    }

    private getAuthHeaders(userId: string, token: string): HttpHeaders {
        return new HttpHeaders()
            .set("X-ALP-UserID", userId)
            .set("X-ALP-Token", token);
    }

    public async createUser(userId: string, password: string): Promise<boolean> {
        const body = {
            user_id: userId,
            password: password,
        }
        const res = await this.doPost('/users', body);
        if(res.status!='ok') {
          return false;
        }
        return true;
    }

    public async getUser(userId: string, token: string): Promise<User> {
        await TimeHelper.sleep(2000);
        return new User();
    }

    public async login(userId: string, password: string): Promise<string> {
        const body = {
            user_id: userId,
            password: password,
        }
        const res = await this.doPost('/users/_login', body);
        return res.token;
    }
    public async getUserResource(userId: string, token: string): Promise<UserResource> {
        const res = await this.doGet('/users/resource',{
          headers: this.getAuthHeaders(userId, token),
        });

        const userResource = new UserResource();
        userResource.plan = res.plan;
        userResource.traderMax = res.trader_max;
        userResource.traderSaved = res.trader_saved;
        return userResource;
    }


    public async getCharts(exchangeId: number, duration: number,
                           trading: string, settlement: string,
                           start: Date, end: Date): Promise<Chart[]> {
        const params = new HttpParams()
            .set('exchange_id', exchangeId.toString())
            .set('duration', duration.toString())
            .set('trading', trading.toString())
            .set('settlement', settlement.toString())
            .set('start', start.toISOString())
            .set('end', end.toISOString());

        const res = await this.doGet('/charts', {
            params: params
        });

        const charts: Chart[] = [];
        for(const item of res) {
            const chart = new Chart();
            chart.last = Number.parseFloat(item.last);
            chart.open = Number.parseFloat(item.open);
            chart.high = Number.parseFloat(item.high);
            chart.low = Number.parseFloat(item.low);
            chart.volume = Number.parseFloat(item.volume);
            chart.datetime = new Date(Date.parse(item.datetime));
            charts.push(chart);
        }

        charts.sort((a, b) => {
            const at = a.datetime.getTime();
            const bt = b.datetime.getTime();
            if(a < b) return -1;
            if(a > b) return 1;
            return 0;
        });

        return charts;
    }

    public async getExchanges(): Promise<Exchange[]> {
        const res = await this.doGet('/exchanges');

        const exchanges: Exchange[] = [];
        for(const item of res) {
            const exchange = new Exchange();
            exchange.id = item.id;
            exchange.name = item.name;
            exchanges.push(exchange);
        }

        return exchanges;
    }
    public async getSettings(userId: string, token: string): Promise<Setting[]> {
        const res = await this.doGet('/settings', {
            headers: this.getAuthHeaders(userId, token),
        });
        const settings: Setting[] = [];
        for(const item of res) {
            const setting = new Setting({
                settingId: item.setting_id,
                exchangeId: item.exchange_id,
                apiKey: item.api_key,
                secretKey: ""
            });
            settings.push(setting);
        }
        return settings;
    }

    public async setSetting(setting: Setting, userId: string, token: string): Promise<boolean> {
        const reqObj = setting.toJson();
        const res = await this.doPost('/settings', reqObj, {
            headers: this.getAuthHeaders(userId, token)
        });
        if(res.status != "ok") {
          return false;
        }
        return true;
    }

    public async deleteSetting(setting_id: number, userId: string, token: string): Promise<boolean> {
        const res = await this.doDelete('/settings/'+setting_id, {
            headers: this.getAuthHeaders(userId, token),
        });

        if(res.status != "ok") {
          return false;
        }
        return true;
    }

    public async getCurrencyPairs(exchangeId: number): Promise<CurrencyPair[]> {
        const res = await this.doGet('/exchanges/' + exchangeId + "/currency_pairs");

        const currencyPairs: CurrencyPair[] = [];
        for(const item of res) {
            const currencyPair = new CurrencyPair(item.trading, item.settlement);
            currencyPairs.push(currencyPair);
        }

        return currencyPairs;
    }

    public async runBacktest(backtest: Backtest, userId: string, token: string): Promise<{backtestId: number, status: string}> {
        const reqObj = backtest.toJson();
        const res = await this.doPost('/backtests', reqObj, {
            headers: this.getAuthHeaders(userId, token)
        });

        return {
            backtestId: res.backtest_id,
            status: res.status,
        };
    }

    public async getBacktestResult(backtestId: number, userId: string, token: string): Promise<BacktestStatus> {
        const res = await this.doGet('/backtests/' + backtestId, {
            headers: this.getAuthHeaders(userId, token),
        });

        if(res.status == "finished" || res.status == "aborted") {
            return BacktestResult.fromJson(res);
        } else {
            return res.status;
        }
    }

    public async runTrader(trader: Trader, userId: string, token: string): Promise<{traderId: number, status: string}> {
        const reqObj = trader.toJson();
        const res = await this.doPost('/traders', reqObj, {
            headers: this.getAuthHeaders(userId, token)
        });

        return {
            traderId: res.trader_id,
            status: res.status,
        };
    }

    public async patchTrader(traderId: number,trader: Trader, userId: string, token: string): Promise<boolean> {
        const reqObj = trader.toJson();
        const res = await this.doPatch('/traders/'+traderId, reqObj, {
            headers: this.getAuthHeaders(userId, token)
        });
        if(res.status != "ok") {
          return false;
        }
        return true;
    }

    public async queueStopTrader(traderId: number, userId: string, token: string): Promise<boolean> {
        const res = await this.doPost('/traders/'+traderId+'/stop', {}, {
            headers: this.getAuthHeaders(userId, token)
        });
        if(res.status != "ok") {
          return false;
        }
        return true;
    }

    public async queueStartTrader(traderId: number, userId: string, token: string): Promise<boolean> {
        const res = await this.doPost('/traders/'+traderId+'/start', {}, {
            headers: this.getAuthHeaders(userId, token)
        });
        if(res.status != "ok") {
          return false;
        }
        return true;
    }

    public async deleteTrader(traderId: number, userId: string, token: string): Promise<boolean> {
        const res = await this.doDelete('/traders/'+traderId, {
            headers: this.getAuthHeaders(userId, token),
        });

        if(res.status != "ok") {
          return false;
        }
        return true;
    }

    public async getTraders(userId: string, token: string): Promise<{
        traderId: number,
        status: string,
        tickSec: number,
        terminateReason: string,
        description: string,
    }[]> {
        const res = await this.doGet('/traders', {
            headers: this.getAuthHeaders(userId, token),
        });
        return res.map(function(r){
          return {traderId: r.trader_id, status: r.status, tickSec: r.tick_sec, terminateReason: r.terminate_reason, description: r.description};
        });
    }

    public async getTrader(traderId: number,userId: string, token: string): Promise<TraderStatus> {
        const res = await this.doGet('/traders/' + traderId,{
            headers: this.getAuthHeaders(userId, token),
        });

        return TraderStatus.fromJson(res);
    }

    public async getOrdersByTraderId(traderId: number,userId: string, token: string): Promise<Order[]> {
        const res = await this.doGet('/traders/' + traderId + '/orders',{
            headers: this.getAuthHeaders(userId, token),
        });

        const  orders: Order[] = [];
        for(const item of res) {
            const order = Order.fromJson(item);
            orders.push(order);
        }

        return orders;
    }

    public async getLogsByTraderId(traderId: number,userId: string, token: string): Promise<Log[]> {
        const res = await this.doGet('/traders/' + traderId + '/logs',{
            headers: this.getAuthHeaders(userId, token),
        });

        const logs: Log[] = [];
        for(const item of res) {
            const log = new Log({datetime: new Date(parseInt(item.datetime)), line:item.line});
            logs.push(log);
        }

        return logs;
    }
}
