import { Injectable } from '@angular/core';
import { Backtest, BacktestStatus } from '../models';
import { HttpService } from './http.service';
import { UserService } from './user.service';

export type BacktestResult = "Running" | BacktestStatus;

@Injectable()
export class BacktestService {

    constructor(private httpService: HttpService,
                private userService: UserService) { }

    public async runBacktest(backtest: Backtest): Promise<{backtestId: number, status: string}>  {
        return await this.httpService.runBacktest(backtest,
                                                  this.userService.getUserId(),
                                                  this.userService.getToken());
    }

    public async getBacktestResult(backtestId: number): Promise<BacktestStatus> {
        return await this.httpService.getBacktestResult(
            backtestId, this.userService.getUserId(), this.userService.getToken(),
        )
    }
}
