import { Injectable } from '@angular/core';
import { Trader,TraderStatus,Order,Log } from '../models';
import { HttpService } from './http.service';
import { UserService } from './user.service';

@Injectable()
export class TraderService {

    constructor(private httpService: HttpService,
                private userService: UserService) { }

    public async runTrader(trader: Trader): Promise<{traderId: number, status: string}>  {
        return await this.httpService.runTrader(trader,
                                                  this.userService.getUserId(),
                                                  this.userService.getToken());
    }

    public async getTraders(): Promise<{
        traderId: number,
        status: string,
        tickSec: number,
        terminateReason: string,
        description: string,
    }[]> {
        return await this.httpService.getTraders(
            this.userService.getUserId(), this.userService.getToken(),
        );
    }

    public async getTrader(traderId: number): Promise<TraderStatus> {
        return await this.httpService.getTrader(
            traderId,
            this.userService.getUserId(), this.userService.getToken(),
        );
    }

    public async getOrders(traderId: number): Promise<Order[]> {
        return await this.httpService.getOrdersByTraderId(
            traderId,
            this.userService.getUserId(), this.userService.getToken(),
        );
    }
    public async getLogs(traderId: number): Promise<Log[]> {
        return await this.httpService.getLogsByTraderId(
            traderId,
            this.userService.getUserId(), this.userService.getToken(),
        );
    }
    public async queueStopTrader(traderId: number): Promise<boolean> {
        return await this.httpService.queueStopTrader(
            traderId,
            this.userService.getUserId(), this.userService.getToken(),
        );
    }
    public async deleteTrader(traderId: number): Promise<boolean> {
        return await this.httpService.deleteTrader(
            traderId,
            this.userService.getUserId(), this.userService.getToken(),
        );
    }

    public async patchTrader(traderId:number, trader: Trader): Promise<boolean> {
        return await this.httpService.patchTrader(
            traderId, trader,
            this.userService.getUserId(), this.userService.getToken(),
        );
    }

}
