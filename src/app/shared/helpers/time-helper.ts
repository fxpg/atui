export class TimeHelper {
    public static sleep(msec: number): Promise<boolean> {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(true);
            }, msec);
        });
    }
}
