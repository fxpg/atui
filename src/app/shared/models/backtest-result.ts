import { Backtest } from './backtest';

export type BacktestStatus = "running" | "pending" | BacktestResult;

export class BacktestLog {
    datetime: Date;
    message: string;
    level: string;
    line: string;

    public static fromJson(json: any): BacktestLog {
        return new BacktestLog({
            datetime: json.datetime,
            message: json.message,
            level: json.level,
            line: json.line,
        });
    }

    constructor(data: {
        datetime: Date,
        message: string,
        level: string,
        line: string,
    }) {
        this.datetime = data.datetime;
        this.message = data.message;
        this.level = data.level;
        this.line = data.line;
    }
}

export class BacktestOrder {
    datetime: Date;
    price: number;

    public static fromJson(json: any): BacktestOrder {
        return new BacktestOrder({
            datetime: json.datetime,
            price: json.price,
        });
    }

    constructor(data: {
        datetime: Date,
        price: number,
    }) {
        this.datetime = data.datetime;
        this.price = data.price;
    }
}

export class BacktestPosition {
    open: BacktestOrder;
    close: BacktestOrder;

    public static fromJson(json: any): BacktestPosition {
        return new BacktestPosition({
            open: BacktestOrder.fromJson(json.open),
            close: BacktestOrder.fromJson(json.close),
        });
    }

    constructor(data: {
        open: BacktestOrder,
        close: BacktestOrder,
    }) {
        this.open = data.open;
        this.close = data.close;
    }
}

export class BacktestResult {
    status: string;
    backtestInfo: Backtest;
    positions: BacktestPosition[];
    logs: BacktestLog[];
    elapsedTimeSecs: number;

    public static fromJson(json: any): BacktestResult {
        return new BacktestResult({
            status: json.status,
            backtestInfo: Backtest.fromJson(json.backtest_info),
            positions: json.positions.map(BacktestPosition.fromJson),
            logs: json.logs.map(BacktestLog.fromJson),
            elapsedTimeSecs: json.elapsed_time_secs,
        })
    }

    constructor(data: {
        status: string,
        backtestInfo: Backtest,
        positions: BacktestPosition[],
        logs: BacktestLog[],
        elapsedTimeSecs: number,
    }) {
        this.status = data.status;
        this.backtestInfo = data.backtestInfo;
        this.positions = data.positions;
        this.logs = data.logs;
        this.elapsedTimeSecs = data.elapsedTimeSecs;
    }
}
