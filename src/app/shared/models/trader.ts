export class Trader {
    description: string;
    code: string;
    tickSec: number;

    public static fromJson(json: any) {
        return new Trader({
            description: json.description,
            code: json.code,
            tickSec: json.tick_sec,
        });
    }

    constructor(data: {description: string,
        code: string, tickSec: number
    }) {
        this.description = data.description;
        this.code = data.code;
        this.tickSec = data.tickSec;
    }

    public toJson(): any {
        return {
            description: this.description,
            code: this.code,
            tick_sec: this.tickSec,
        };
    }
}

export class Log {
  datetime: Date;
  line: string;

  public static fromJson(json: any) {
      return new Log({
        datetime: new Date(json.datetime),
        line: json.line
      });
  }

  constructor(data: {datetime:Date,line:string,
  }) {;
    this.datetime = data.datetime;
    this.line = data.line;
  }
}

export class Order {
  orderId: number;
  exchange: string;
  pair : string;
  exchangeOrderId: string;
  datetime: Date;
  status:boolean;
  price: number;
  amount:number;
  tradeType: string;

    public static fromJson(json: any) {
        return new Order({
          orderId: json.orderId,
          exchange: json.exchange,
          pair : json.pair,
          exchangeOrderId: json.exchangeOrderId,
          datetime: json.datetime,
          status: json.status,
          price: json.price,
          amount: json.amount,
          tradeType: json.tradeType
        });
    }

    constructor(data: {orderId: number,exchange:string,pair:string,exchangeOrderId:string,datetime:Date,
        status:boolean, price:number, amount:number, tradeType:string
    }) {
      this.orderId = data.orderId;
      this.exchange = data.exchange;
      this.pair = data.pair;
      this.exchangeOrderId = data.exchangeOrderId;
      this.datetime = data.datetime;
      this.status = data.status;
      this.price = data.price;
      this.amount = data.amount;
      this.tradeType = data.tradeType;
    }

    public toJson(): any {
        return {
          order_id: this.orderId,
          exchange: this.exchange,
          pair : this.pair,
          exchangeOrderId: this.exchangeOrderId,
          datetime: this.datetime,
          status:this.status,
          price: this.price,
          amount:this.amount,
          tradeType: this.tradeType
        };
    }
}
