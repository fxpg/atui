export class Backtest {
    exchangeId: number;
    from: Date;
    to: Date;
    lossCutRate: number;
    profitTakeRate: number;
    trading: string;
    settlement: string;
    code: string;
    tickSec: number;

    public static fromJson(json: any) {
        return new Backtest({
            exchangeId: json.exchange_id,
            from: json.from,
            to: json.to,
            lossCutRate: json.loss_cut_rate,
            profitTakeRate: json.profit_take_rate,
            trading: json.trading,
            settlement: json.settlement,
            code: json.yaml,
            tickSec: json.tick_sec,
        });
    }

    constructor(data: {
        exchangeId: number, from: Date, to: Date,
        lossCutRate: number, profitTakeRate: number,
        trading: string, settlement: string,
        code: string, tickSec: number,
    }) {
        this.exchangeId = data.exchangeId;
        this.from = data.from;
        this.to = data.to;
        this.lossCutRate = data.lossCutRate;
        this.profitTakeRate = data.profitTakeRate;
        this.trading = data.trading;
        this.settlement = data.settlement;
        this.code = data.code;
        this.tickSec = data.tickSec;
    }

    public toJson(): any {
        return {
            exchange_id: this.exchangeId,
            from: this.from,
            to: this.to,
            loss_cut_rate: this.lossCutRate,
            profit_take_rate: this.profitTakeRate,
            trading: this.trading,
            settlement: this.settlement,
            yaml: this.code,
            tick_sec: this.tickSec,
        };
    }
}
