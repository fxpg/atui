export class CurrencyPair {
    pair: string;
    trading: string;
    settlement: string;

    constructor(trading: string, settlement: string) {
        this.trading = trading;
        this.settlement = settlement;
        this.pair = settlement + '/' + trading;
    }
}
