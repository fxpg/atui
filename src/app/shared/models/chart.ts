export class Chart {
    last: number
    open: number
    high: number
    low: number
    volume: number
    datetime: Date
}
