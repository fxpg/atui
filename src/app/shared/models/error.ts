enum ErrorCode {
    ER001 = '不正なリクエストです',
    ER002 = 'アクセス権限がありません',
    ER101 = '同じユーザ名が既に存在します',
    ER102 = 'ユーザIDがありません',
    ER103 = 'パスワードがありません',
    ER104 = 'ユーザIDが正しくありません 5〜16文字で設定してください',
    ER105 = 'パスワードが正しくありません 英数字記号で8文字以上を使用してください',
    ER106 = '新たに設定するパスワードが正しくありません',
    ER107 = '認証に失敗しました',
    ER108 = 'ユーザ情報の取得に失敗しました',
    ER109 = 'ログインに失敗しました',
    ER110 = '不正な入力です',
    ER111 = 'パスワードの変更に失敗しました 時間をおいて再度試してください',
    ER112 = '設定の変更に失敗しました 時間をおいて再度試してください',
    ER113 = 'APIKeyがありません',
    ER114 = 'SecretKeyがありません',
    ER115 = '取引所に対して登録できるAPIキーは1つのみです',
    ER301 = '取引所が不正です',
    ER501 = '取引ボットの作成上限です',
    ER502 = '取引ボットは起動しておりません',
    ER503 = '取引ボットは起動中のため削除できません。',
    ER504 = '取引ボットは停止移行中のため削除できません。時間をおいて再度試してください',
    ER999 = 'アクセスが集中してます 時間をおいて再度試してください',
}
const keys = Object.keys(ErrorCode);
const values = keys.map(k => ErrorCode[k as any]);

export class Error {
    constructor(public errorCode: string, public message: string) {
    }

    public errorCodeToString(): string {
      for (const {item, index} of keys.map((item, index) => ({ item, index }))) {
        if (item === 'ER'+String(this.errorCode)) {
          return values[index]
        }
      }
      return "不明なエラーが発生しました"
    }
}
