import { Trader } from './trader';

export class TraderStatus {
    traderId: number;
    description: string;
    code:string;
    tickSec: number;
    status: string;
    terminateReason:string;

    public static fromJson(json: any) {
        return new TraderStatus({
            traderId: json.trader_id,
            description: json.description,
            code: json.code,
            tickSec: json.tick_sec,
            status: json.status,
            terminateReason: json.terminate_reason
        });
    }

    constructor(data: {traderId: number,
        description: string, code: string, tickSec: number,
        status: string, terminateReason:string,
    }) {
        this.traderId = data.traderId;
        this.description = data.description;
        this.code = data.code;
        this.tickSec = data.tickSec;
        this.status= data.status;
        this.terminateReason = data.terminateReason;
    }

    public toJson(): any {
        return {
            trader_id: this.traderId,
            description: this.description,
            code: this.code,
            tick_sec: this.tickSec,
            status: this.status,
            terminate_reason: this.terminateReason,
        };
    }
}
