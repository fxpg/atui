export class User {
    userId: string;
}

export class UserResource {
  plan: string;
  traderMax: number;
  traderSaved: number;
}
