export class Setting {
    settingId: number
    exchangeId: number
    apiKey: string
    secretKey: string

    constructor(data: {
        settingId: number,
        exchangeId: number,
        apiKey: string, secretKey: string,
    }) {
        this.settingId = data.settingId;
        this.exchangeId = data.exchangeId;
        this.apiKey = data.apiKey;
        this.secretKey = data.secretKey;
    }

    public toJson(): any {
        return {
            exchange_id: this.exchangeId,
            api_key: this.apiKey,
            secret_key: this.secretKey,
        }
    }
}
