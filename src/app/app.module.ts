import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { SectionsModule } from './sections/sections.module';
import { ComponentsModule } from './components/components.module';
import { ExamplesModule } from './alphatrader/alphatrader.module';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import * as services from './shared/services';

import { AppComponent } from './app.component';
import { LoginComponent } from './alphatrader/login/login.component';
import { PresentationComponent } from './presentation/presentation.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { TraderComponent } from './alphatrader/trader/trader.component';
import { MarkdownModule } from 'ngx-md';

import { PresentationModule } from './presentation/presentation.module';
@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        NgbModule.forRoot(),
        MarkdownModule.forRoot(),
        FormsModule,
        RouterModule,
        AppRoutingModule,
        PresentationModule,
        SectionsModule,
        ComponentsModule,
        ExamplesModule,

        HttpModule,
        HttpClientModule,

    ],
    providers: [
        services.HttpService,
        services.UserService,
        services.ChartService,
        services.ExchangeService,
        services.BacktestService,
        services.TraderService,
        services.ToasterService,
        services.SettingService,
      ],
    bootstrap: [AppComponent]
})
export class AppModule { }
